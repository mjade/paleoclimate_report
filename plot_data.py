from matplotlib.transforms import Transform
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
from scipy.stats import spearmanr
import pickle
import matplotlib.pyplot as plt
plt.rcParams['text.usetex'] = True


# Part 1 - Read Data
gurke = pickle.load(open('./pickles/eine_grosse_gurke.pickle', 'rb'))
# print(gurke.keys()) # to see all data frames; keys correspond to tha name of the xlsx file

# Gilderoy
gilderoy = gurke['Stalagmite Data class 2021.xlsx']
print(gilderoy.keys())
age_gilderoy = gilderoy['age']
y_name1 = 'd13C init -5'  # not sure I should be using these columns
y_name11 ='d13Cinit -8'
y_name2 = 'd18O'
y_name3 = '25 -> 25  Mg  [ H2 ] '

# Antarctic temperature from Deuterium
antarctic_temperature_data = gurke['Antarctic_temperature_from_Deuterium.xlsx']
age_antarctt = antarctic_temperature_data[antarctic_temperature_data.keys()[0]][10:]*1000
antarctt_degC = antarctic_temperature_data[antarctic_temperature_data.keys()[1]][10:]

# Antarctic Methane 
antarctic_methane_data = gurke['Antarctica_Methane.xlsx']
age_methane = antarctic_methane_data[antarctic_methane_data.keys()[1]][20:]
methane_ppbv = antarctic_methane_data[antarctic_methane_data.keys()[2]][20:]

# Composite CO2 record
composite_co2_data = gurke['CO2_composite_ice_core.xlsx']
age_co2 = composite_co2_data['Composite CO2 record (0-800 kyr BP)'][16:] 
co2_ppmv = composite_co2_data['Unnamed: 1'][16:]

# Equatorial Pacific SST and Mg/Ca
eqpac_data = gurke['EqPac_MgCa_SST.xlsx']
age_eqpac = eqpac_data['Age [ka BP]']*1000 
sst_eqpac = eqpac_data['SST (1-12) [Â°C]'] 
# from here, we might also take G. ruber d18O [per mil PDB]	G. ruber Mg/Ca [mmol/mol]	SST (1-12) [Â°C]	d18O H2O [per mil SMOW]

# Sc/Ca
# skipped for now

# Benthic d18O from Lisiecki and Raymo 2005
benthic_d18O_data = gurke['Lisiecki and Raymo 2005.xlsx']
age_benthic_d18O= benthic_d18O_data['Time 0-5.3 Myr (ka)']*1000
benthic_d18O_permil = benthic_d18O_data['  Benthic d18O (per mil)  ']

# Relative sea level (RSL)
rsl_data = gurke['Rohling_2014_Red_Sea_Level.xlsx'] 
age_rsl = rsl_data[rsl_data.keys()[0]][15:]*1000
rsl_m = rsl_data[rsl_data.keys()[1]][15:]

# Iberian SST from Hodell 
iberian_sst_hodell = gurke['SST_Iberian_Margin.xlsx']
age_sst_iberia = iberian_sst_hodell['Unnamed: 2'][21:]*1000  # original units were [ka BP]
sst_iberia = iberian_sst_hodell['Unnamed: 3'][21:] # SST (1-12) [degC]

# Wang, Cheng
# skipped for now


# # Part 2- Interpolate data to the age levels of Gilderoy
# # Important note: Gilderoy time levels aren't as dense as those of the other data, you might want to define denser ones and interpolate to them.
# def get_corr_coef(age_data, var_data, var_string):
#     f1 = interp1d(age_data, var_data, kind='linear')
#     var_interp = f1(age_gilderoy)
#     # g3_mask = [gilderoy[y_name3] != np.nan][0]
#     # print(g3_mask)
#     corr_coeff1 = spearmanr(gilderoy[y_name1], var_interp, nan_policy='omit')
#     corr_coeff2 = spearmanr(gilderoy[y_name2], var_interp, nan_policy='omit')
#     # corr_coeff3 = spearmanr(gilderoy[y_name3][g3_mask], var_interp[g3_mask], nan_policy='omit')
#     print('%s %.2f %.2f' % (var_string, corr_coeff1[0], corr_coeff2[0])) #,  corr_coeff3[0])

# get_corr_coef(age_antarctt, antarctt_degC, 'antarctt_degC')
# get_corr_coef(age_methane, methane_ppbv, 'methane')
# get_corr_coef(age_co2, co2_ppmv, 'co2')
# get_corr_coef(age_eqpac, sst_eqpac, 'eq_pac_sst')
# get_corr_coef(age_benthic_d18O, benthic_d18O_permil, 'benthic_d18O')
# get_corr_coef(age_rsl, rsl_m, 'rsl')
# get_corr_coef(age_sst_iberia, sst_iberia, 'sst_iberia')


discussion = False # if plot is used for discussion or result section (false)
if discussion:
    nrows = 8
    figsize = (8,12)
    name = 'discussion_' + np.str(figsize)
else:
    nrows = 3
    figsize = (8,8)
    name = 'results_' + np.str(figsize)

# Part 3 - Plotting 
fig, axs = plt.subplots(nrows=nrows, ncols=1, figsize=figsize, sharex=True, gridspec_kw={'wspace': 0.05, 'hspace': 0.05})

fontsize=9

#axs[0].plot(age_gilderoy, gilderoy[y_name1], label=y_name1)
#axs[0].plot(age_gilderoy, gilderoy[y_name11], label=y_name11)
axs[0].fill_between(age_gilderoy,gilderoy[y_name1], gilderoy[y_name11])
axs[0].set_ylabel(r'GLD $\delta^{13}C$ [\textperthousand]', fontsize=fontsize)
axs[0].set_ylim([-14,-3])
axs[0].invert_yaxis()
axs[1].plot(age_gilderoy, gilderoy[y_name3], label='GLD Mg/Ca [mmol/mol]')
axs[1].set_ylabel('GLD Mg/Ca [mmol/mol]', fontsize=fontsize)
axs[1].set_ylim([16,48])

axs[2].plot(age_gilderoy, gilderoy[y_name2], label=y_name2)
axs[2].set_ylabel(r'GLD $\delta^{18}O$ [\textperthousand]', fontsize=fontsize)
axs[2].invert_yaxis()


if discussion:
    axs[3].plot(age_sst_iberia, sst_iberia)  
    axs[3].set_ylabel('SST Iberia [degC]', fontsize=fontsize)
    axs[4].plot(age_co2, co2_ppmv)
    axs[4].set_ylabel('CO$_2$ [ppmv]', fontsize=fontsize)
    axs[4].set_ylim([180,310])
#    axs[5].plot(age_eqpac, sst_eqpac)
#    axs[5].set_ylabel('Eq. Pac. SSR [degC]', fontsize=6)
    axs[5].plot(age_benthic_d18O, benthic_d18O_permil)
    axs[5].set_ylabel(r'Benthic $\delta^{18}O$ [\textperthousand]', fontsize=fontsize)
    axs[5].invert_yaxis()
    axs[6].plot(age_antarctt, antarctt_degC)
    axs[6].set_ylabel('Antarctic T [degC]', fontsize=fontsize)
#    axs[8].plot(age_methane, methane_ppbv)
#    axs[8].set_ylabel('CH_4 [ppbv]', fontsize=6)
    axs[7].plot(age_rsl, rsl_m)
    axs[7].set_ylabel('RSL [m]', fontsize=fontsize)



phase_a_min, phase_a_max = 138000, 152000
phase_b_min, phase_b_max = 132000, 138000 
phase_c_min, phase_c_max = 116000, 132000

letters = [' a', ' b', ' c', ' d', ' e', ' f', ' g', ' h', ' i', ' j', ' k', ' l']
letter_i = 0
for ax in axs:
    ax.set_xlim(95778, 150635)
    ax.get_xaxis().set_visible(False)
    ax.axvspan(phase_a_min, phase_a_max, alpha=0.2, color='red')
    ax.axvspan(phase_b_min, phase_b_max, alpha=0.2, color='green')
    ax.axvspan(phase_c_min, phase_c_max, alpha=0.2, color='orange')
    ax.text(0.01, 0.95, letters[letter_i], transform=ax.transAxes, va='top', ha='left')
    letter_i += 1
# plt.legend()

axs[0].text(phase_a_max-2000, -13, 'Phase A', va='top', ha='right')
axs[0].text(phase_b_max-700, -13, 'Phase B', va='top', ha='right')
axs[0].text(phase_c_max-700, -13, 'Phase C', va='top', ha='right')
axs[0].text(phase_c_min-700, -13, 'Phase D', va='top', ha='right')

axs[nrows-1].get_xaxis().set_visible(True)
axs[nrows-1].set_xlabel('Age [yr BP]')
axs[nrows-1].ticklabel_format(axis='x', scilimits=(3, 3), useMathText=True)
plt.tight_layout()
#plt.show()

fig.savefig(f'fig/{name}.png', dpi=200)
