import pandas as pd
import os
import pickle

filenames = os.listdir('./xlsx/')
print(filenames)

out_dict = {}
for filename in filenames:
    print(filename)
    df_file = pd.read_excel('./xlsx/' + filename)
    out_dict.update({filename: df_file})

pickle.dump(out_dict, file=(open('./pickles/eine_grosse_gurke.pickle', 'wb')))
