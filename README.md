# Paleoclimate Report


In order to read the `.xlsx` files, I needed `pandas==1.3.5`, `openpyxl==3.0.9`,
which I couldn't get to work on my IAC PC.
Therefore I wrote `add_xlsx_to_pickles.py` to put them all in one big
python dict, the keys of which correspond to the names of the `.xslx` files and
the values - to pandas data frames, the dict is saved to a pickle file.

`plot_data.py` loads that pickle file and plays with the data and doesn't
depend on such a recent version of pandas. Work in progress...

The directory `./xlsx/` contains the Datasets used in the orbital tour,
downloaded from moodle and data from Gilderoy (Stalagmite data class 2021).
Almost all of them are saved to the big pickle file.

The directory `./not_read_xlsx/` includes a few excel files I couldn't read and
probably won't because of laziness

## File list
"Antarctica_Methane.xlsx" | Loulergue et al., Nature, 2008 (added) |

"Antarctic_temperature_from_Deuterium.xlsx" | Parrenin et al. (2013) (added) |

"CO2_composite_ice_core.xlsx" - Composite CO2 record, references for our period
(160-90 ka BP) are: 60-115 kyr BP:	EDML (Bereiter et al., 2012); 105-155 kyr BP:	Dome C Sublimation (Schneider et al., 2013); 155-393 kyr BP:	Vostok (Petit et al., 1999). (added) |

"EqPac_MgCa_SST.xlsx" - Equatorial Pacific SSTs, there's also d18O H2O, no
citation inside (added only SSTs, not d18O) |

"Li_Loess_Sr-Ca.xlsx" | Smoothed microcodium Sr/Ca, from Xifeng and from Zhenyuan, no idea what this
is (skipped) |

"Lisiecki and Raymo 2005.xlsx" | Benthic d18O (added) |

"Rohling_2014_Red_Sea_Level.xlsx" | Grant et al. (2012) | includes lower and
upper bounds (added only middle value) |

"SST_Iberian_Margin.xlsx" | Hodel et al. (2013), Iberian SSTs (added) |

"Stalagmite Data class 2021.xlsx" | Gilderoy (added) |

"Wang_et_al_Botuvera.xlsx"  | Inside says Cruz et al.i (2005), whatever, has insolation
driven changes in subtropical Brazil, variables are d13C and d18O (skipped) |

"Cheng_asia_composite.xlsx" | can't read, no idea what this is (skipped) |

"IRD SImon et al 2013.xls"  | can't read, no idea what this is (skipped) |

![plot](./Figure_1.png)
